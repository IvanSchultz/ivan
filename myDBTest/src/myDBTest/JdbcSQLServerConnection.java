package myDBTest;
 
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
 
/**
 * This program demonstrates how to establish database connection to Microsoft
 * SQL Server.
 * @author www.codejava.net
 *
 */
public class JdbcSQLServerConnection {
 
    public static void main(String[] args) throws ClassNotFoundException {
 
        Connection conn = null;
                		
        try {
 
        	String dbURL = "jdbc:jtds:sqlserver://PROD18SQL.shoprite.co.za:57020/PRODBAM;instance=PROD16SQL1;domain=shoprite.co.za";
            String user = "mq";
            String pass = "4urAGi6U";
            Connection conn1 = DriverManager.getConnection(dbURL, user, pass);
            
            if (conn1 != null) {
                DatabaseMetaData dm = (DatabaseMetaData) conn1.getMetaData();
                System.out.println("Driver name: " + dm.getDriverName());
                System.out.println("Driver version: " + dm.getDriverVersion());
                System.out.println("Product name: " + dm.getDatabaseProductName());
                System.out.println("Product version: " + dm.getDatabaseProductVersion());
            }
 
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (conn != null && !conn.isClosed()) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}